#!/usr/bin/env sh
while true; do
  feh --randomize --bg-fill ${HOME}/home/celso/.fluxbox/background/*
  sleep 300
done