" ~/.vimrc

call plug#begin('~/.vim/plugged')

" Lista de plugins

call plug#end()

source $VIMRUNTIME/defaults.vim
" Tamanho de linha e indentação
autocmd FileType text setlocal textwidth=72
autocmd FileType markdown setlocal textwidth=72
autocmd FileType markdown setlocal autoindent
" Usar Pathogen
" execute pathogen#infect()
" Configurações de indentação:
syntax enable
set title
set mouse=a
set incsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
autocmd FileType make setlocal noexpandtab
" Cores
set bg=light
colorscheme murphy
" Configurações de linhas
set nu
"set cul
set so=5
" Atalhos para copiar e colar no clipboard
map \y "+y
map \p "*p
" Navegador netrw
let g:netrw_banner=0






